# compile-kernel-ubuntu

Command line python script for downloading, compiling, building, installing linux kernel *.DEB file from sources kernel.org.

Tested on Ubuntu 20.04.

First install:
```bash
sudo apt install python3 python3-feedparser
```
Script may runs system update with 
`apt-get update && apt-get -y upgrade`.
And may install dependencies packages for building:
`apt-get install git kernel-package fakeroot build-essential libncurses5-dev python3-pip wget axel xz-utils flex bison libssl-dev libelf-dev python3-feedparser rsync`

Usage with `sudo`:

```bash
sudo python3  compile-kernel-ubuntu.py
                [--help] - print this message
                [--source-dir="/usr/src"] - directory for building, default "/usr/src" storage for source files in Ubuntu.
                [--download-kernel] - download kernel source from kernel.org.
                [--build-install] - build and install new compiled kernel.
                [--version="x.x.x"] - set linux source version, can be found on kernel.org.
                [--type="stable|mainline|longterm"] - set linux source branch.               
                [--build-method="normal|debian"] - build type:
                    "normal" - by 'make', 'make modules_install', 'make install' commands;
                    "debian" - by 'make-kpkg' command.
                [--install-dependencies] - install dependencies packages for building, runs once.
```   

Simply usage example for download, compile, install the latest kernel:
```bash
sudo python3 compile-kernel-ubuntu.py --build_install --download_kernel
```

Another simply usage example for compile, install the latest kernel:
```bash
sudo python3 compile-kernel-ubuntu.py --build_install
```

And with source dir setup:
```bash
sudo python3 compile-kernel-ubuntu.py --build_install --download_kernel --source_dir="path-to-build"
```