#!/usr/bin/python
from subprocess import call
import os
import sys
import getopt
from os import chdir
from functions.functions import validate_kernel_type, validate_build_method, get_release_candidate_version, get_stable_version, get_cpu_count, install_kernel, find_required_parameter
import tarfile
import time

from shutil import copyfile


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


warn_message_build_dir = bcolors.WARNING + '  Use default parameter for' + \
    bcolors.OKBLUE + ' --source-dir="/usr/src" \n' + bcolors.ENDC


def main(argv):
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "",
            [  # ht:v:m:db
                "help", "source-dir=", "type=", "version=", "build-method=",
                "download-kernel", "build-install", "install-dependencies"
            ])

    except getopt.GetoptError as err:
        print(bcolors.FAIL + "  " + err.__str__() + bcolors.ENDC)
        print(bcolors.WARNING + '''  Usage: ''' + bcolors.OKGREEN +
              '''compile-kernel-ubuntu.py ''' + bcolors.WARNING + '''
        --help 
        --source-dir="/dir-to-kernel-source/" 
        --type="stable|mainline|longterm" 
        --version="x.x.x" 
        --build-method="debian|normal" 
        --download-kernel 
        --build-install 
        --install-dependencies''' + bcolors.ENDC)
        sys.exit(2)

    arg = [""]

    # Check required parameter "--source-dir" exists.
    if not find_required_parameter(opts, "--source-dir"):
        print(warn_message_build_dir)
        opts.insert(0, ("--source-dir", "/usr/src"))

    to_clean_sources = ''
    # Check has the meaning to do some work, if not print warn.
    if (not find_required_parameter(opts, "--build-install")) and (
            not find_required_parameter(opts, "--download-kernel")):
        print(bcolors.WARNING + "  Not set " + bcolors.OKBLUE +
              "--build-install " + bcolors.WARNING + "or " + bcolors.OKBLUE +
              "--download-kernel " + bcolors.WARNING +
              "parameters. Nothing to do with kernel sources!" + bcolors.ENDC)
        time.sleep(10)
    elif find_required_parameter(opts, "--build-install"):
        while (to_clean_sources != 'y') or (to_clean_sources != 'n'):
            to_clean_sources = input(
                "  Will cleaning up kernel sources after compiling? (y/n):")
            if to_clean_sources == 'y':
                print(bcolors.OKGREEN +
                      "\n  Will cleaning up after compiling..." + bcolors.ENDC)
                break
            elif to_clean_sources == "n":
                print(bcolors.OKGREEN +
                      "\n  Will NOT cleaning up after compiling..." +
                      bcolors.ENDC)
                break
        time.sleep(1)

    # print(opts)
    # Sort parameters for processing by desc
    opts = sorted(opts, reverse=True)
    # print(opts)

    # apt-get update?
    to_apt_get_update = ''
    while (to_apt_get_update != 'y') or (to_apt_get_update != 'n'):
        to_apt_get_update = input("\n  Update system before compiling? (y/n):")
        if to_apt_get_update == 'y':
            print(bcolors.OKGREEN + "\n  Updating system..." + bcolors.ENDC)
            print(bcolors.OKGREEN +
                  "\n  Begining update system with apt-get...\n" +
                  bcolors.ENDC)
            call("apt-get update && apt-get -y upgrade", shell=True)
            break
        elif to_apt_get_update == "n":
            print(bcolors.OKGREEN + "\n  Skip update system..." + bcolors.ENDC)
            break

    # Default values.
    BUILD_DIR = ""
    testing = ""
    kernel_type = ""
    build_method = ""
    kernel_version = ""

    if not find_required_parameter(opts, "--type"):
        print(bcolors.OKGREEN + "\n  Use default parameter for " +
              bcolors.OKBLUE + "--type=stable" + bcolors.OKGREEN +
              ". Stable kernel release.\n" + bcolors.ENDC)
        kernel_type = 'stable'

    if not find_required_parameter(opts, "--build-method"):
        print(bcolors.OKGREEN + "\n  Use default parameter for " +
              bcolors.OKBLUE + "--build-method=debian" + bcolors.OKGREEN +
              ". For *.DEB package creating.\n" + bcolors.ENDC)
        build_method = 'debian'

    if not find_required_parameter(opts, "--version"):
        print(bcolors.OKGREEN + "\n  Use default parameter for " +
              bcolors.OKBLUE + "--version=None" + bcolors.OKGREEN +
              ". Latest release.\n" + bcolors.ENDC)
        kernel_version = None

    if kernel_type == 'mainline':
        if kernel_version == None:  # no version was suplied as arg
            kernel_version = get_release_candidate_version()
        rc = kernel_version.find('-rc')
        if rc > 0:
            testing = '/testing'
    else:
        if kernel_version == None:
            kernel_version = get_stable_version()

    cpu_count = get_cpu_count()
    print(bcolors.OKGREEN + "\n  CPU count:" + bcolors.OKBLUE +
          " %s\n" % cpu_count + bcolors.ENDC)
    if cpu_count > 1:
        print(bcolors.OKGREEN + "  Use multithreading! \n" + bcolors.ENDC)
    else:
        print(bcolors.OKGREEN + "  Use single thread! \n" + bcolors.ENDC)

    # print("opt=%s arg=%s" % opts, arg)

    for opt, arg in opts:
        print(bcolors.OKGREEN + '\n   Parameter processing: ' +
              bcolors.OKBLUE + opt + ('' if arg == '' else '="') + arg +
              ('' if arg == '' else '"') + '\n' + bcolors.ENDC)

        if opt in ("--install-dependencies"):
            print(
                bcolors.OKGREEN +
                "\n  Begining install dependencies packages for building with apt-get...\n"
                + bcolors.ENDC)
            call(
                "apt-get install git kernel-package fakeroot build-essential libncurses5-dev python3-pip wget axel xz-utils flex bison libssl-dev libelf-dev python3-feedparser rsync",
                shell=True)
            print(
                bcolors.OKGREEN +
                "\n\n  Begining install python support script software packages with pip install...\n"
                + bcolors.ENDC)
            call("pip3 install feedparser sh", shell=True)

        elif opt in ("--source-dir"):
            BUILD_DIR = arg
            if BUILD_DIR == '':
                print(warn_message_build_dir)

            if (os.path.isdir(BUILD_DIR)) == True:
                print(bcolors.OKGREEN + "\n  Go to directory: " +
                      bcolors.OKBLUE + BUILD_DIR + "\n" + bcolors.ENDC)
                chdir(BUILD_DIR)
            else:
                print(bcolors.OKGREEN + "\n  Create directory: " +
                      bcolors.OKBLUE + BUILD_DIR + "\n" + bcolors.ENDC)
                call("mkdir -p %s" % BUILD_DIR, shell=True)

        elif opt in ("-h", "--help"):
            print(
                bcolors.OKGREEN +
                '''  compile-kernel-ubuntu.py -h[--help] - print this message. 
                [--source-dir="dir-to-kernel-source"] - directory for building, must be set.
              -t[--type="stable|mainline|longterm"] - set linux source branch.
              -v[--version="x.x.x"] - set linux source version, can be found on kernel.org.
              -m[--build-method="normal|debian"] - build type:
                    "normal" - by 'make', 'make modules_install', 'make install' commands;
                    "debian" - by 'make-kpkg' command.
              -d[--download-kernel] - download kernel source from kernel.org.
              -b[--build-install] - build and install new compiled kernel.
                [--install-dependencies] - install dependencies packages for building, runs once.'''
                + bcolors.ENDC)
            sys.exit()

        elif opt in ("-t", "--type"):
            isValidKernelType = validate_kernel_type(arg)
            if (isValidKernelType == True):
                kernel_type = arg
            else:
                print(bcolors.WARNING + "  Invalid or No Kernel type entered" +
                      bcolors.ENDC)
                print(
                    bcolors.OKGREEN +
                    "  Valid options for the '--type' parameter are : 'stable' or 'mainline' or 'longterm'\n"
                    + bcolors.ENDC)
                sys.exit()

        elif opt in ("-v", "--version"):
            kernel_version = arg

        elif opt in ("-m", "--build-method"):
            isValidBuildMethod = validate_build_method(arg)
            if (isValidBuildMethod == True):
                build_method = arg
            else:
                print(bcolors.WARNING + "  Invalid build-method entered" +
                      bcolors.ENDC)
                print(
                    bcolors.OKGREEN +
                    "  Valid options for the 'build-method' parameter are  : 'debian' or 'normal'\n"
                    + bcolors.ENDC)
                sys.exit()

        elif opt in ("-d", "--download-kernel"):
            if BUILD_DIR == "":
                print(warn_message_build_dir)
                sys.exit()

            ret_tar_xz_extract_status = 2  # err status untar.xz
            file_name_source = "linux-" + kernel_version + ".tar.xz"

            while (ret_tar_xz_extract_status != 0):
                if os.path.isfile(BUILD_DIR + '/' + file_name_source) == False:

                    #    chdir(BUILD_DIR)
                    call(
                        # "wget --continue http://kernel.org/pub/linux/kernel/v4.x%s/linux-%s.tar.xz"
                        "axel --output='%s' -a -n 5 http://kernel.org/pub/linux/kernel/v%s.x%s/%s"
                        % (file_name_source, kernel_version[0], testing,
                           file_name_source),
                        shell=True)
                    print(bcolors.OKGREEN + "  Extracting kernel sources: " +
                          bcolors.OKBLUE + "%s" % kernel_version + " ...\n" +
                          bcolors.ENDC)
                    ret_tar_xz_extract_status = call("tar -Jxf " +
                                                     file_name_source,
                                                     shell=True)
                else:
                    print(bcolors.OKGREEN + "  Found file: " + bcolors.OKBLUE +
                          file_name_source + " \n" + bcolors.ENDC)
                    print(bcolors.OKGREEN + "  Extracting kernel sources: " +
                          bcolors.OKBLUE + "%s" % kernel_version + " ...\n" +
                          bcolors.ENDC)

                    ret_tar_xz_extract_status = call("tar -Jxf " +
                                                     file_name_source,
                                                     shell=True)

                    # print(ret_tar_extract_status)
                    # Checking untarxz file kernel status.
                    if ret_tar_xz_extract_status == 0:
                        print(bcolors.OKGREEN +
                              "  Extracted kernel sources: " + bcolors.OKBLUE +
                              "%s" % kernel_version + bcolors.OKGREEN +
                              " status OK \n" + bcolors.ENDC)
                        # call("tar -Jxf linux-%s.tar.xz" %
                        #      kernel_version, shell=True)
                    else:
                        print(bcolors.OKBLUE +
                              "\n  Kernel sources archive decompress: " +
                              bcolors.FAIL + file_name_source +
                              " status FAIL:" + " ...\n" + bcolors.ENDC)
                        print(bcolors.OKGREEN +
                              "\n  Redownloading kernel sources: " +
                              bcolors.OKBLUE + "%s" % kernel_version +
                              " ...\n" + bcolors.ENDC)

                        call("rm " + BUILD_DIR + "/" + file_name_source,
                             shell=True)
                        call(
                            # "wget --continue http://kernel.org/pub/linux/kernel/v4.x%s/linux-%s.tar.xz"
                            "axel --output='%s' -a -n 5 http://kernel.org/pub/linux/kernel/v%s.x%s/%s"
                            % (file_name_source, kernel_version[0], testing,
                               file_name_source),
                            shell=True)

            print(bcolors.OKGREEN +
                  "\n  Go to directory with kernel sources: " +
                  bcolors.OKBLUE + "%s\n" % kernel_version + bcolors.ENDC)
            chdir("linux-%s" % kernel_version)

            # Download patch for kernel
            if os.path.isfile(BUILD_DIR + '/linux-' + kernel_version +
                              '/patch-' + kernel_version + '.xz') == False:
                print(bcolors.OKGREEN +
                      "\n  Downloading patch for kernel sources: " +
                      bcolors.OKBLUE + "%s\n" % kernel_version + bcolors.ENDC)
                call(
                    "axel -a -n 5 http://kernel.org/pub/linux/kernel/v%s.x%s/patch-%s.xz"
                    % (kernel_version[0], testing, kernel_version),
                    shell=True)
            else:
                print(bcolors.OKGREEN + "  Found file: " + bcolors.OKBLUE +
                      "patch-" + kernel_version + ".xz \n" + bcolors.ENDC)

            print(bcolors.OKGREEN + "  Extracting patch for kernel sources: " +
                  bcolors.OKBLUE + "%s" % kernel_version + " ...\n" +
                  bcolors.ENDC)
            call("unxz patch-%s.xz" % kernel_version, shell=True)
            print(bcolors.OKGREEN + "  Applying patch for kernel sources: " +
                  bcolors.OKBLUE + "%s" % kernel_version + " ...\n" +
                  bcolors.ENDC)
            call("patch -p1 -R <patch-%s" % kernel_version, shell=True)
            call("patch -p1 -i patch-%s" % kernel_version, shell=True)

            current_kernel = os.uname()[2].rstrip('\n')
            print(bcolors.OKGREEN + "  Current kernel version is : " +
                  bcolors.BOLD + "%s\n" % current_kernel + bcolors.ENDC)
            # Start by cleaning up
            # call("make distclean; make mrproper", shell=True)

        elif opt in ("-b", "--build-install"):
            if BUILD_DIR == "":
                print(warn_message_build_dir)
                sys.exit()

            if os.path.isdir(BUILD_DIR + "/linux-%s" % kernel_version) == True:
                print(bcolors.OKGREEN +
                      "\n  Go to directory with kernel sources: " +
                      bcolors.OKBLUE + "%s\n" % kernel_version + bcolors.ENDC)
                chdir(BUILD_DIR + "/linux-%s" % kernel_version)
                # print("  Copying .config from current kernel to target build kernel version : %s\n" %
                #       current_kernel)
                # copyfile("/boot/config-%s" % current_kernel, "./.config")
                print(bcolors.OKGREEN + "\n  Starting make oldconfig..." +
                      bcolors.ENDC)
                call("make oldconfig", shell=True)
                print(bcolors.OKGREEN + "\n  Starting menuconfig...\n" +
                      bcolors.ENDC)
                call("make menuconfig", shell=True)
                # call("make xconfig", shell=True)

                print(bcolors.OKGREEN + "\n  Compiling kernel: " +
                      bcolors.OKBLUE + "%s\n" % kernel_version + bcolors.ENDC)
                # if os.path.isdir("linux-%s" % kernel_version) == True:
                #     chdir("linux-%s" % kernel_version)
                if (build_method == 'debian'):
                    print(bcolors.OKGREEN + "  Building by the Debian method" +
                          bcolors.ENDC)
                    # call("make clean", shell=True)
                    # new_env = os.environ.copy()
                    # os.environ["CONCURENCY_LEVEL"] = "%s" % cpu_count
                    call(
                        "fakeroot make-kpkg -j " + (str(cpu_count)) +
                        " --initrd --append-to-version=-vanillaice kernel_image kernel_headers",
                        shell=True)

                    # Clean up
                    if to_clean_sources == 'y':
                        print(bcolors.OKGREEN + "  Start by cleaning up..." +
                              bcolors.ENDC)
                        call("make-kpkg clean", shell=True)
                    elif to_clean_sources == 'n':
                        print(bcolors.OKGREEN + "  Skip by cleaning up..." +
                              bcolors.ENDC)

                    print(
                        bcolors.OKGREEN +
                        "\n  Begin installing the new compiled Headers and Kernel image: "
                        + bcolors.OKBLUE + "%s\n" % kernel_version +
                        bcolors.ENDC)

                    status = install_kernel(BUILD_DIR, kernel_version)
                    if status == 0:
                        print(
                            bcolors.OKGREEN +
                            "\n  Done installing the new compiled Kernel and Headers, please reboot.\n"
                            + bcolors.ENDC)
                    elif status == 1:
                        print(
                            bcolors.WARNING +
                            "\n  Error: not found the new compiled Kernel deb file: "
                            + bcolors.OKBLUE + BUILD_DIR + '/linux-image-' +
                            kernel_version + '-vanillaice_' + kernel_version +
                            "-vanillaice-10.00.Custom_amd64.deb" +
                            bcolors.ENDC + bcolors.WARNING + " to install!" +
                            bcolors.ENDC)
                    elif status == 2:
                        print(
                            bcolors.WARNING +
                            "\n  Error: not found the new compiled Headers deb file: "
                            + bcolors.OKBLUE + BUILD_DIR + "/linux-headers-" +
                            kernel_version + "-vanillaice_" + kernel_version +
                            "-vanillaice-10.00.Custom_amd64.deb" +
                            bcolors.ENDC + bcolors.WARNING + " to install!" +
                            bcolors.ENDC)
                else:
                    print(bcolors.OKGREEN +
                          "\n  Building by the Normal method" + bcolors.ENDC)
                    # The below commands can be merged into one
                    call("make", shell=True)
                    call("make modules_install", shell=True)
                    call("make install", shell=True)
            else:
                print(bcolors.FAIL +
                      "\n  Error: not found kernel source dir: " +
                      bcolors.OKBLUE + BUILD_DIR +
                      "/linux-%s" % kernel_version + bcolors.ENDC)

        else:
            print(bcolors.FAIL + "\n  Unhandled option: " + opt + bcolors.ENDC)


if __name__ == "__main__":
    main(sys.argv[1:])
