from feedparser import parse
from subprocess import check_output, call
import os
from os import chdir


def get_stable_version():
    feed = parse('https://www.kernel.org/feeds/kdist.xml')
    for item in feed.entries:
        if (item.title.count('stable') == 1):
            kernel_version = item.title.split(':')[0]
            return kernel_version


def get_release_candidate_version():
    feed = parse('https://www.kernel.org/feeds/kdist.xml')
    for item in feed.entries:
        if (item.title.count('mainline') == 1):
            kernel_version = item.title.split(':')[0]
            return kernel_version


def get_cpu_count():
    out = check_output("grep -c  processor /proc/cpuinfo", shell=True)
    return int(out)


def install_kernel(BUILD_DIR, kernel_version):
    sub = "."
    count_dots = kernel_version.count(sub, 0, len(kernel_version))
    if count_dots == 1:
        position = kernel_version.find('-rc')
        if position > 0:
            kernel_version = kernel_version[:position] + \
                '.0' + kernel_version[position:]
        else:
            kernel_version = kernel_version + ".0"

    if (os.path.isfile(BUILD_DIR + '/linux-image-' + kernel_version +
                       '-vanillaice_' + kernel_version +
                       '-vanillaice-10.00.Custom_amd64.deb') == True):
        call(
            "sudo dpkg -i " + BUILD_DIR +
            "/linux-image-%s-vanillaice_%s-vanillaice-10.00.Custom_amd64.deb" %
            (kernel_version, kernel_version),
            shell=True)
    else:
        return 1

    if (os.path.isfile(BUILD_DIR + '/linux-headers-' + kernel_version +
                       '-vanillaice_' + kernel_version +
                       '-vanillaice-10.00.Custom_amd64.deb') == True):
        call(
            "sudo dpkg -i " + BUILD_DIR +
            "/linux-headers-%s-vanillaice_%s-vanillaice-10.00.Custom_amd64.deb"
            % (kernel_version, kernel_version),
            shell=True)
    else:
        return 2
    return 0


def validate_kernel_type(kernel_type):
    if (len(kernel_type) > 0):
        if ((kernel_type == "stable") or (kernel_type == "mainline")
                or (kernel_type == "longterm")):
            return True
        else:
            return False
    else:
        return False


def validate_build_method(build_method):
    if (len(build_method) > 0):
        if ((build_method == "debian") or (build_method == "normal")):
            return True
        else:
            return False
    else:
        return False


def find_required_parameter(opts, find_str):
    for opt in opts:
        # print(find_str in opt)
        # print(opt)
        # print(find_str)
        if find_str in opt:
            return True
    return False
